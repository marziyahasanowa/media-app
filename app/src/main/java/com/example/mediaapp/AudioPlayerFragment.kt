package com.example.mediaapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.mediaapp.databinding.FragmentAudioBinding


class AudioPlayerFragment : Fragment() {

    private lateinit var binding: FragmentAudioBinding
    private val viewModel by activityViewModels<MainViewModel>()
    private var isPlaying: Boolean? = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAudioBinding.inflate(layoutInflater)

        val playPauseButton = binding.audioPlayPause
        val audioFilePath = binding.audioFilepath


        viewModel.filePath.observe(viewLifecycleOwner) {
            val filePath = it ?: ""
            audioFilePath.text = filePath
        }

        viewModel.isPlaying.observe(viewLifecycleOwner){
            isPlaying = it
        }

        viewModel.playPauseText.observe(viewLifecycleOwner){
            binding.audioPlayPause.text = resources.getString(it)
        }

        playPauseButton.setOnClickListener {
                if (isPlaying == true) {
                    pause()
                } else {
                    play()
                }
        }


        return binding.root
    }

    private fun play() {
        viewModel.play()
    }

    private fun pause() {
        viewModel.pause()
    }
//
//    override fun onPause() {
//        if (viewModel.isPlaying.value == true) viewModel.player.pause()
//        super.onPause()
//    }

}

