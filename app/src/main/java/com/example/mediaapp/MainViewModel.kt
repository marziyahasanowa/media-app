package com.example.mediaapp

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.media3.common.MediaItem
import androidx.media3.common.MimeTypes
import androidx.media3.common.Player

import androidx.media3.exoplayer.ExoPlayer
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val context = getApplication<Application>()

    private val _filePath = MutableLiveData<String>()
    val filePath: LiveData<String> get() = _filePath

    private val _isPlaying = MutableLiveData<Boolean>()
    val isPlaying: LiveData<Boolean> get() = _isPlaying

    private val _playPauseText = MutableLiveData<Int>()
    val playPauseText: LiveData<Int> get() = _playPauseText

    private var _totalDuration = MutableLiveData<Int>()
    val totalDuration: LiveData<Int> = _totalDuration

    private var _currentPosition = MutableLiveData<Int>()
    val currentPosition: LiveData<Int> = _currentPosition

    lateinit var player: ExoPlayer

    init {
        initializePlayer()
    }

    private fun initializePlayer() {
        player = ExoPlayer.Builder(context).build()
    }

    fun play() {
        player.play()
        _isPlaying.value = true
        _playPauseText.value = R.string.pause
    }

    fun pause() {
        player.pause()
        _isPlaying.value = false
        _playPauseText.value = R.string.play
    }

    fun seekTo(progress: Long) {
        player.seekTo(progress)
    }

    @androidx.annotation.OptIn(androidx.media3.common.util.UnstableApi::class)
    fun setFilePath(path: String) {
        _filePath.value = path

        val mediaItem = MediaItem.Builder()
            .setUri(path)
            .setMimeType(MimeTypes.AUDIO_MP4)
            .build()
        player.setMediaItem(mediaItem)
        player.prepare()
        player.addListener(object : Player.Listener {
            @Deprecated("Deprecated in Java")
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playbackState == Player.STATE_READY) {
                    _totalDuration.value = player.duration.toInt()
                    _currentPosition.value = player.currentPosition.toInt()

                }
                if (playbackState == Player.STATE_READY && playWhenReady) {
                    viewModelScope.launch {
                        while (true) {
                            _currentPosition.value = player.currentPosition.toInt()
                            delay(1000)
                        }
                    }
                }
            }

            @Deprecated("Deprecated in Java")
            override fun onPositionDiscontinuity(reason: Int) {
                if (reason == Player.EVENT_POSITION_DISCONTINUITY) {
                    _currentPosition.value = player.currentPosition.toInt()
                }
            }
        })
    }

    override fun onCleared() {
        if (::player.isInitialized) {
            player.release()
        }
        super.onCleared()
    }

}

