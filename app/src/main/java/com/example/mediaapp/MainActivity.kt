package com.example.mediaapp

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.mediaapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private companion object {
        private const val REQUEST_AUDIO_FILE = 1
        private const val REQUEST_VIDEO_FILE = 2
    }

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(application))[MainViewModel::class.java]

        val playAudioButton = binding.playAudioButton
        val playVideoButton = binding.playVideoButton

        playAudioButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "audio/*"
            startActivityForResult(intent, REQUEST_AUDIO_FILE)
        }

        playVideoButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "video/*"
            startActivityForResult(intent, REQUEST_VIDEO_FILE)
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && data != null) {
            val selectedFileUri: Uri = data.data!!

            val filePath = selectedFileUri.toString()

            viewModel.setFilePath(filePath)

            when (requestCode) {
                REQUEST_AUDIO_FILE -> showAudioPlayerFragment(filePath)
                REQUEST_VIDEO_FILE -> showVideoPlayerFragment(filePath)
            }
        }
    }

    private fun showAudioPlayerFragment(filePath: String) {
        val fragment = AudioPlayerFragment()
        supportFragmentManager.beginTransaction()
            .replace(binding.fragmentContainer.id, fragment)
            .addToBackStack(null)
            .commit()
    }


    private fun showVideoPlayerFragment(filePath: String) {
        val fragment = VideoPlayerFragment()
        supportFragmentManager.beginTransaction()
            .replace(binding.fragmentContainer.id, fragment)
            .addToBackStack(null)
            .commit()
    }
}


