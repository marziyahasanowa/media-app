package com.example.mediaapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.media3.ui.PlayerView
import com.example.mediaapp.databinding.FragmentVideoBinding


class VideoPlayerFragment : Fragment() {

    private lateinit var binding: FragmentVideoBinding
    private lateinit var playerView: PlayerView
    private val viewModel by activityViewModels<MainViewModel>()
    private var isPlaying: Boolean? = false
    private lateinit var seekBar: SeekBar
    private lateinit var progressDuration: TextView
    private var savedSeekBarProgress: Int = 0

    @androidx.annotation.OptIn(androidx.media3.common.util.UnstableApi::class)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentVideoBinding.inflate(layoutInflater)

        val playPauseButton = binding.videoPlayPause

        viewModel.filePath.observe(viewLifecycleOwner) {
            playerView = binding.videoView
            playerView.player = viewModel.player
        }

        viewModel.isPlaying.observe(viewLifecycleOwner) {
            isPlaying = it
        }

        viewModel.playPauseText.observe(viewLifecycleOwner) {
            binding.videoPlayPause.text = resources.getString(it)
        }

        playPauseButton.setOnClickListener {
            if (isPlaying == true) {
                pause()
            } else {
                play()
            }
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        seekBar = binding.seekbar
        progressDuration = binding.progressDuration

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    viewModel.seekTo(progress.toLong())
                }
                Log.d("VideoPlayerFragment", "Seek bar progress changed to $progress")
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
    }

    override fun onStart() {
        super.onStart()
        viewModel.totalDuration.observe(viewLifecycleOwner) { duration ->
            seekBar.max = duration
            viewModel.currentPosition.observe(viewLifecycleOwner) { position ->
                val formattedText = formatDuration(position) + "/" + formatDuration(duration)
                progressDuration.text = formattedText
                if (!seekBar.isPressed) {
                    seekBar.progress = position
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("seekBarProgress", seekBar.progress)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        savedInstanceState?.let {
            savedSeekBarProgress = it.getInt("seekBarProgress", 0)
        }
    }

    override fun onResume() {
        super.onResume()
        seekBar.progress = savedSeekBarProgress
    }
    private fun play() {
        viewModel.play()
    }

    private fun pause() {
        viewModel.pause()
    }

    private fun formatDuration(duration: Int): String {
        val minutes = (duration / 1000) / 60
        val seconds = (duration / 1000) % 60
        return String.format("%02d:%02d", minutes, seconds)
    }

}